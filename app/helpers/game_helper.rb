module GameHelper
  def image_of_hand(hand)
    image_tag(hand.to_s + '.gif')
  end
end
